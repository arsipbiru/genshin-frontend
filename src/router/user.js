export default [
    {
        path: '/user/inventory/:type',
        name: 'user-inventory',
        component: () => import('@/views/user/inventory/index.vue'),
        meta: { 
            layout: 'full',
            menuCollapsed: true, 
        },
    },  
    {
        path: '/user/tier-list',
        name: 'user-tier-list',
        component: () => import('@/views/user/tier_list/index.vue'),
        meta: { 
            layout: 'full',
            menuCollapsed: true, 
        },
    }, 
    {
        path: '/user/builds',
        name: 'user-builds',
        component: () => import('@/views/user/builds/index.vue'),
        meta: { 
            layout: 'full',
            menuCollapsed: true, 
        },
    }, 
]