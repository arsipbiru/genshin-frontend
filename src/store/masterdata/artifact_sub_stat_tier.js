import baseURL from '@/static/baseURL'
import Cookies from 'universal-cookie'  

const cookies = new Cookies()

const state = {
    ARSST_DT: [],
    error: null,
    isLoading: false
};

const mutations = {
    UPD_DT_ARSST(state, data) {
        state.ARSST_DT = {...state.ARSST_DT, ...data}
        state.error = null
        state.isLoading = false
    },
    UPD_DT_ARSST_INV(state, data) {
        state.ARSST_INV_DT = {...state.ARSST_INV_DT, ...data}
        state.error = null
        state.isLoading = false
    },
    SUCCESS(state) {
        state.isLoading = false
    },
    LOADING(state) {
        state.isLoading = true
    },
    ERROR(state, error) {
        state.error = error
    },
    RESETERROR(state){
        state.error = null
    }
};

const getters = {
    reducedFormat(state){
        let temp = {}
        state.ARSST_INV_DT?.data?.map(item => {
            if(!temp[item.arsst_stat_key]) temp[item.arsst_stat_key] = {}
            if(!temp[item.arsst_stat_key][item.arsst_rarity]) temp[item.arsst_stat_key][item.arsst_rarity] = {}

            temp[item.arsst_stat_key][item.arsst_rarity][item.score] = item.arsst_tier
        })
        return temp
    }
}

const actions = {
    getAll({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('masterdata/artifact_sub_stat_tier', config)  
                commit('UPD_DT_ARSST', data);
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    getForInventory({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('inventory/artifacts/sub_stat_tiers', config)  
                commit('UPD_DT_ARSST_INV', data);
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    save({ commit }, payload) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.post('masterdata/artifact_sub_stat_tier', payload, config)  
                commit('SUCCESS');
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    delete({ commit }, payload) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.delete(`masterdata/artifact_sub_stat_tier/${payload}`, config)  
                commit('SUCCESS');
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}