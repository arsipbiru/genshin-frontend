import baseURL from '@/static/baseURL'
import Cookies from 'universal-cookie'  

const cookies = new Cookies()

const state = {
    STATS_DT: [],
    error: null,
    isLoading: false
};

const mutations = {
    UPD_DT_STATS(state, data) {
        state.STATS_DT = {...state.STATS_DT, ...data}
        state.error = null
        state.isLoading = false
    },
    SUCCESS(state) {
        state.isLoading = false
    },
    LOADING(state) {
        state.isLoading = true
    },
    ERROR(state, error) {
        state.error = error
    },
    RESETERROR(state){
        state.error = null
    }
};

const getters = {
    reducedFormat(state){
        let temp = {}
        state.STATS_DT?.data?.map(item => {
            temp[item.stat_key] = item
        })
        return temp
    },
    getRaw(state){
        return state.STATS_DT?.data
    }
}

const actions = {
    getAll({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('masterdata/stats', config)  
                commit('UPD_DT_STATS', data);
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    save({ commit }, payload) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.post('masterdata/stats', payload, config)  
                commit('SUCCESS');
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    delete({ commit }, payload) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.delete(`masterdata/stats/${payload}`, config)  
                commit('SUCCESS');
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}