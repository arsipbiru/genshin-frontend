import baseURL from '@/static/baseURL'
import Cookies from 'universal-cookie'  

const cookies = new Cookies()

const state = {
    ARSET_DT: [],
    error: null,
    isLoading: false
};

const mutations = {
    UPD_DT_ARSET(state, data) {
        state.ARSET_DT = {...state.ARSET_DT, ...data}
        state.error = null
        state.isLoading = false
    },
    SUCCESS(state) {
        state.isLoading = false
    },
    LOADING(state) {
        state.isLoading = true
    },
    ERROR(state, error) {
        state.error = error
    },
    RESETERROR(state){
        state.error = null
    }
};

const getters = {
    dt(state){
        return state.ARSET_DT.data
    }
}

const actions = {
    getAll({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('masterdata/artifact_set', config)  
                commit('UPD_DT_ARSET', data);
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    save({ commit }, payload) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.post('masterdata/artifact_set', payload, config)  
                commit('SUCCESS');
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    delete({ commit }, payload) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.delete(`masterdata/artifact_set/${payload}`, config)  
                commit('SUCCESS');
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}