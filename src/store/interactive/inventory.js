import baseURL from '@/static/baseURL'
import Cookies from 'universal-cookie'  

const cookies = new Cookies()

const state = {
    IVAR_CURRENT: null,
    error: null,
    isLoading: false
};

const mutations = {
    UPD_IVAR_CURRENT(state, data) {
        state.IVAR_CURRENT = {...state.IVAR_CURRENT, ...data}
        state.error = null
        state.isLoading = false
    },
    SUCCESS(state) {
        state.isLoading = false
    },
    LOADING(state) {
        state.isLoading = true
    },
    ERROR(state, error) {
        state.error = error
    },
    RESETERROR(state){
        state.error = null
    },
    RESETSTATE(){
        state.IVAR_CURRENT = null
    },
};

const actions = {
    resetState({commit}){
        commit('RESETSTATE')
    },
    getArtifacts({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('inventory/artifacts', config)  
                commit('UPD_IVAR_CURRENT', {artifact:data.data});
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data.data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    getWeapons({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('inventory/weapons', config)  
                commit('UPD_IVAR_CURRENT', {weapon:data.data});
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data.data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    getCharacters({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('inventory/characters', config)  
                commit('UPD_IVAR_CURRENT', {chara:data.data});
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data.data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}