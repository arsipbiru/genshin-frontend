import baseURL from '@/static/baseURL'
import Cookies from 'universal-cookie'  

const cookies = new Cookies()

const state = {
    BUILD_STATE: null,
    error: null,
    isLoading: false
};

const mutations = {
    UPD_BUILD_CURRENT(state, data) {
        state.BUILD_STATE = {...state.BUILD_STATE, ...data}
        state.error = null
        state.isLoading = false
    },
    SUCCESS(state) {
        state.isLoading = false
    },
    LOADING(state) {
        state.isLoading = true
    },
    ERROR(state, error) {
        state.error = error
    },
    RESETSTATE(){
        state.BUILD_STATE = null
    },
    RESETERROR(state){
        state.error = null
    }
};

const actions = {
    resetState({commit}){
        commit('RESETSTATE')
    },
    getCharacterArtifact({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('inventory/artifacts', config)  
                commit('UPD_BUILD_CURRENT', {artifact:data.data});
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data.data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    getCharacterInfo({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('builds/character-info', config)  
                commit('UPD_BUILD_CURRENT', {info: data.data});
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data.data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
    getCharacterWeapon({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params, headers: { authorization: `bearer ${cookies.get('token')}` } }
                const {data} = await baseURL.get('builds/character-weapon', config)  
                commit('UPD_BUILD_CURRENT', {weapon: data.data});
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data.data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}