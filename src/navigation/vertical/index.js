export default [
  {
    header: 'User\'s Menu',
  },
  {
    title: 'Inventory',
    route: 'user-inventory',
    icon: 'HomeIcon',
  }, 
  {
    title: 'Tier List',
    route: 'user-tier-list',
    icon: 'HomeIcon',
  }, 
  {
    title: 'Builds',
    route: 'user-builds',
    icon: 'HomeIcon',
  },  
  {
    header: 'Misc',
  },  
]
